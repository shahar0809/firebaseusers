import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from '../../services/auth.service';
import { FormGroup, FormControl, FormGroupDirective, NgForm,
   Validators, FormBuilder } from '@angular/forms';

const MIN_PASSWORD_LEN = 8;

@Component({
  selector: 'app-sign-up',
  templateUrl: './sign-up.component.html',
  styleUrls: ['./sign-up.component.css'],
})

export class SignUpComponent implements OnInit {
  /* Two way binding to the html input elements */
  email: string="";
  password: string="";

  hide = true; // Defined for the password visibility icon

  /* Form control to validate the email field */
  emailFormControl = new FormControl('', 
  Validators.compose([Validators.required, Validators.email]));

  /* Form control to validate the password field */
  passwordFormControl = new FormControl('', 
  Validators.compose([Validators.required, Validators.minLength(8)]));

  constructor(private router:Router, private auth:AuthService, ) {}

  ngOnInit(): void {}

  submitSignUp() {
      this.auth.signUp(this.email, this.password).then((result) => {
        // Navigate to welcome when sign up process is finished
        this.router.navigate(['/welcome'])
      })
      // Handling firebase errors
      .catch((error) => { window.alert(error.message);})
  }

  signIn() {
    this.router.navigate(['/signin'])
  }
}

import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from '../../services/auth.service';
import {FormControl, FormGroupDirective, NgForm, Validators} from '@angular/forms';
import {ErrorStateMatcher} from '@angular/material/core';
//import { MIN_PASSWORD_LEN } from '../sign-up/sign-up.component';

export class EmailErrorStateMatcher implements ErrorStateMatcher {
  isErrorState(control: FormControl | null, form: FormGroupDirective | NgForm | null): boolean {
    const isSubmitted = form && form.submitted;
    return !!(control && control.invalid && (control.dirty || control.touched || isSubmitted));
  }
}

@Component({
  selector: 'app-sign-in',
  templateUrl: './sign-in.component.html',
  styleUrls: ['./sign-in.component.css']
})
export class SignInComponent implements OnInit {
  /* Two way binding to the html input elements */
  email: string="";
  password: string="";

  hide = true; // Defined for the password visibility icon

  /* Form control to validate the email field */
  emailFormControl = new FormControl('', 
  Validators.compose([Validators.required, Validators.email]));

  /* Form control to validate the password field */
  passwordFormControl = new FormControl('', 
  Validators.compose([Validators.required, Validators.minLength(8)]));

  constructor(private router:Router, private auth:AuthService) {}

  ngOnInit(): void {}

  submitSignIn() {
    this.auth.signIn(this.email, this.password).then((result) => {
      // Navigate to welcome when sign in process is finished
      this.router.navigate(['/welcome']);
    })
    // Handling firebase errors
    .catch((error) => { window.alert(error.message);})
  }

  signUp() {
    this.router.navigate(['/signup'])
  }

}

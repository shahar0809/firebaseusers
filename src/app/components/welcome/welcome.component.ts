import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from '../../services/auth.service';

@Component({
  selector: 'app-welcome',
  templateUrl: './welcome.component.html',
  styleUrls: ['./welcome.component.css']
})
export class WelcomeComponent implements OnInit {
  
  constructor(private router:Router, private auth:AuthService) {
   }

  ngOnInit(): void {
  }

  /* Logging out of the current user in Firbase */
  logOut() {
      this.auth.logOut().then((result) => { 
        localStorage.removeItem('user');
        this.router.navigate(['/']) 
      })
  }
}

import { Injectable } from '@angular/core';
import { User } from "../services/user";
import { AngularFireAuth } from "@angular/fire/auth";

@Injectable({
  providedIn: 'root'
})

export class AuthService  {
  userData: any; // Current logged user in firebase

  

  constructor(private firebaseAuth: AngularFireAuth) {
    this.userData = this.firebaseAuth.authState;

    // Listener for logging in/out
    this.firebaseAuth.authState.subscribe(user => {
      /* There is a logged user */
      if (user) {
        this.userData = user;
        localStorage.setItem('user', JSON.stringify(this.userData));
      /* User is logged out */
      } else {
        localStorage.setItem('user', 'null');
      }
    })
   }

   /* Sign up to firebase using email and password */
   signUp(email: string, password: string) : Promise<any> {
     return this.firebaseAuth.createUserWithEmailAndPassword(email, password);
   }

   /* Sign in to firebase using email and password */
   signIn(email: string, password: string) : Promise<any> {
    return this.firebaseAuth.signInWithEmailAndPassword(email, password);
  }

  /* Log user out from firebase */
  logOut() : Promise<any> {
    return this.firebaseAuth.signOut();
  }

}

// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebase: {apiKey: "AIzaSyBNzO0uKMPRqFXREN-G0QikRydV9vGywwk",
  authDomain: "fir-users-6d3e3.firebaseapp.com",
  projectId: "fir-users-6d3e3",
  storageBucket: "fir-users-6d3e3.appspot.com",
  messagingSenderId: "854165053573",
  appId: "1:854165053573:web:c58d14e991952f2bec49ec"}

};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
